//
//  Spring.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Spring.h"

@implementation Spring

-(void) didLoadFromCCB
{
    self.physicsBody.collisionType = @"spring";
}

-(void) springAnimation
{
    // the animation manager of each node is stored in the 'userObject' property
    CCBAnimationManager* animationManager = self.userObject;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"sprung"];
}

@end
