//
//  Player.h
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/15/14.
//
//

#import "TimeDependentSprite.h"

@interface Player : TimeDependentSprite

@property (readwrite, nonatomic) int health;

-(void) startJumpAnimation;
-(void) startWalkAnimation;
-(void) startHurtAnimation;

@end
