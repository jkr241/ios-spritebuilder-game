//
//  TimeDependent.h
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/15/14.
//
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface TimeDependentSprite : CCSprite

-(void) didLoadFromCCB;
-(void) saveCurrentPosition;
-(void) goBackInTime;

@end
