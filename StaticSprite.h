//
//  StaticSprite.h
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/17/14.
//
//

#import "CCSprite.h"

@interface StaticSprite : CCSprite

-(void) update:(CCTime)delta withSpeed:(double) speed;
+(NSObject *) noCollideGroup;

@end
