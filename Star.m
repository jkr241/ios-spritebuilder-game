//
//  Star.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/30/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Star.h"

@implementation Star

-(void) didLoadFromCCB
{
    self.physicsBody.collisionType = @"star";
    self.physicsBody.collisionGroup = [StaticSprite noCollideGroup];
}

@end
