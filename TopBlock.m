//
//  TopBlock.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "TopBlock.h"

@implementation TopBlock

-(void) didLoadFromCCB
{
    self.physicsBody.collisionType = @"topblock";
}

@end
