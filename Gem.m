//
//  Gem.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Gem.h"

@implementation Gem

-(void) didLoadFromCCB
{
    self.physicsBody.collisionType = @"gem";
    self.physicsBody.collisionGroup = [StaticSprite noCollideGroup];
}
@end
