//
//  Gem.h
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "StaticSprite.h"

@interface Gem : StaticSprite

@end
