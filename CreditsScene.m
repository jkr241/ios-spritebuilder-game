//
//  CreditsScene.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/30/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CreditsScene.h"


@implementation CreditsScene
{
    CCNode *_credits;
    CCNode *_returnButton;
    double speed;
    AVAudioPlayer *audioPlayer;
}

-(void) didLoadFromCCB
{
    speed = 50;
    [self displayCredits];
    
    NSURL *backgroundURL = [[NSBundle mainBundle] URLForResource:@"Jason_Shaw_-_Forever_Believe" withExtension:@"mp3"];
    if (backgroundURL != nil)
    {
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundURL error:nil];
        audioPlayer.numberOfLoops = -1; //infinite
    }
    if ([audioPlayer prepareToPlay]) {
        [audioPlayer play];
    }
    
    if ([[CCDirector sharedDirector] isPaused]){
        [[CCDirector sharedDirector] resume];
    }
}

-(void) displayCredits
{
    for (CCNode *credit in [_credits children]) {
        credit.position = ccp(credit.position.x , credit.position.y - 1500);
    }
}

-(void) update:(CCTime)delta
{
    for (CCNode *credit in [_credits children]) {
        credit.position = ccp(credit.position.x, credit.position.y + delta * speed);
    }
    
    if (_returnButton.position.y > [[UIScreen mainScreen] bounds].size.width / 2) {
        [[CCDirector sharedDirector] pause];
    }
}

-(void) returnToMenu
{
    if ([[CCDirector sharedDirector] isPaused]) {
        [[CCDirector sharedDirector] resume];
    }
    [audioPlayer stop];
    CCScene *mainscene =[CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:mainscene];
    
}
@end
