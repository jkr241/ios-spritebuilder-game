//
//  StaticSprite.m
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/17/14.
//
//

#import "StaticSprite.h"

@implementation StaticSprite


-(void) update:(CCTime) delta withSpeed:(double) speed
{
    //Get object's current position, update and set the new value
    self.position = ccp(self.position.x - (delta * speed), self.position.y);
}
+(id) noCollideGroup
{
    static NSObject *theGroup;
    if( theGroup == nil ){
        theGroup = [[NSObject alloc] init];
    }
    return theGroup;
}

@end
