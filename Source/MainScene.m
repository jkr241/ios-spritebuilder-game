//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"

@implementation MainScene
{
    AVAudioPlayer *audioPlayer;
    
    CCButton *_toggleMusicButton;
}

-(void) didLoadFromCCB
{
    NSURL *backgroundURL = [[NSBundle mainBundle] URLForResource:@"Jason_Shaw_-_Big_Car_Theft" withExtension:@"mp3"];
    if (backgroundURL != nil)
    {
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundURL error:nil];
        audioPlayer.numberOfLoops = -1; //infinite
    }
    if ([audioPlayer prepareToPlay]) {
        [audioPlayer play];
    }
}

-(void) loadTutorial
{
    if (audioPlayer.isPlaying) {
        [audioPlayer stop];
    }
    
    CCScene *gameplayScene = [CCBReader loadAsScene:@"TutorialScene"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}

//Start game when player clicks button
-(void) play
{
    if (audioPlayer.isPlaying) {
        [audioPlayer stop];
    }
    
    CCScene *gameplayScene = [CCBReader loadAsScene:@"Gameplay"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
    
}
-(void) viewCredits
{
    [audioPlayer stop];
    CCScene *credits = [CCBReader loadAsScene:@"CreditsScene"];
    [[CCDirector sharedDirector] replaceScene:credits];
}
-(void) pauseMusic
{
    [_toggleMusicButton togglesSelectedState];
    if ([audioPlayer isPlaying]) {
        [audioPlayer pause];
    }
    else{
        [audioPlayer play];
    }
    
}

@end
