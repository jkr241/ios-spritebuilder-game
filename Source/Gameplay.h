//
//  Gameplay.h
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/26/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"
#import "TimeDependentSprite.h"
#import "Player.h"
#import "Enemy.h"
#import "Spring.h"
#import "StaticSprite.h"
#import "TopBlock.h"
#import "BackgroundSprite.h"
#import <UIKit/UISwipeGestureRecognizer.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>



@interface Gameplay : CCNode <UIGestureRecognizerDelegate>

@property (readwrite, nonatomic)BOOL isPlayerJumping;
@property (readwrite, nonatomic)BOOL isReverseEnabled;
@property (readwrite, nonatomic)BOOL isReversingTime;
@property (readwrite, nonatomic)BOOL isPlayerWalking;
@property (readwrite, nonatomic)BOOL playerWasHurt;
@property (readwrite, nonatomic)BOOL hasPlayerUsedSpring;
@property (readwrite, nonatomic)float playerHurtTimer;
@property (readwrite, nonatomic)float playerInvulnerableTimer;
@property (readwrite, nonatomic)float screenWidth;
@property (readwrite, nonatomic)float screenHeight;
@property (readonly, nonatomic)CCPhysicsNode *physicsNode;
@property (readwrite, nonatomic) Player *player;

-(void) didLoadFromCCB;
-(void) hurtPlayer;
-(void) killEnemy;
-(void) collectCoin;
-(void) makePlayerWalk;
-(void) makePlayerJump;
-(void) playerUsedSpring;
-(void) resetSpringTimer;
-(void) collectGem;
-(void) collectStarAtPos:(CGPoint) pos;
-(void) superJump;
-(void) stopMusic;

@end
