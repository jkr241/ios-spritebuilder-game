//
//  Gameplay.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/26/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Gameplay.h"
#import "CollisionHandler.h"


@implementation Gameplay

{
    CCButton *_toggleMusicButton;
    
    //Game variables
    long int score;
    long int highScore;
    int remainingJumpSteps;
    int difficultyThreshold;
    int lastLevelNum;
    double speed;
    BOOL isMusicPaused;
    float timeUntilSpeedUp;
    float lastBlockPos;
    float reverseTimeRemaining;
    float timeSinceLastCoinLevel;
    float timeUntilDifficultyIncrease;
    
    
    NSMutableArray *spritesToDelete;
    NSMutableArray *spritesToAdd;
    
    NSTimer *jumpTimer;
    
    CCPhysicsNode *_physicsNode;
    CollisionHandler *collHand;
    
    //Buffer holding the levels
    CCNode *_levelBuffer;
    //Node displaying the moving background
    CCNode *_backgroundNode;
    
    //HUD for player time reverse remaining
    CCLabelTTF *_rechargeText;
    CCNodeColor *_timeBar;
    
    CCLabelTTF *_scoreDisplay;
    CCLabelTTF *_highScoreDisplay;
    CCLabelTTF *_highScoreText;
    CCNode *_heart1;
    CCNode *_heart2;
    CCNode *_heart3;
    Player *_player;
    
    //Sounds stuff
    NSURL *backgroundURL;
    NSURL *backgroundReversedURL;
    SystemSoundID jumpSound;
    SystemSoundID hurtSound;
    SystemSoundID coinGrabSound;
    SystemSoundID enemyKillSound;
    SystemSoundID gemGrabSound;
    SystemSoundID springJumpSound;
    SystemSoundID superSpringSound;
    SystemSoundID starGrabSound;
    
    AVAudioPlayer *_audioPlayer;
    AVAudioPlayer *_reverseAudioPlayer;
    
    UISwipeGestureRecognizer *recognizer;
    
    
}

// is called when CCB file has completed loading
- (void)didLoadFromCCB
{
    if ([[CCDirector sharedDirector] isPaused]) {
        [[CCDirector sharedDirector] resume];
    }
    // tell this scene to accept touches
    self.userInteractionEnabled = YES;
    //    _physicsNode.debugDraw = YES;
    
    
    //Initializing variables
    spritesToDelete = [NSMutableArray array];
    spritesToAdd = [NSMutableArray array];
    
    _screenWidth = [[CCDirector sharedDirector] viewSize].width;
    _screenHeight = [[CCDirector sharedDirector] viewSize].height;
    
    timeUntilSpeedUp = TIMEUNILSPEEDUP;
    
    //The game node will handle collisions
    collHand = [[CollisionHandler alloc] initWithWorld:self];
    _physicsNode.collisionDelegate = collHand;
    
    //Set up the booleans
    _isPlayerWalking = YES;
    _isReverseEnabled = YES;
    
    //Gesture recognizer for swipes
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(pushPlayerDown)];
    recognizer.numberOfTouchesRequired = 1;
    recognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [[[CCDirector sharedDirector] view] addGestureRecognizer:recognizer];
    
    
    backgroundURL = [[NSBundle mainBundle] URLForResource:@"Jason_Shaw_-_Cycles" withExtension:@"mp3"];
    if (backgroundURL != nil)
    {
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundURL error:nil];
        _audioPlayer.numberOfLoops = -1; //infinite
    }
    backgroundReversedURL = [[NSBundle mainBundle] URLForResource:@"Jason_Shaw_-_Cycles_reversed" withExtension:@"mp3"];
    if (backgroundReversedURL != nil) {
        _reverseAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundReversedURL error:nil];
    }
    
    //Play background music
    [_audioPlayer play];
    
    //Set up the game
    [self drawInitialBackgroundObjects];
    [self loadSounds];
    speed = INITIALSPEED;
    timeSinceLastCoinLevel = COINLEVELTIME;
    timeUntilDifficultyIncrease = DIFFICULTYINCREASETIME;
    difficultyThreshold = 1;
    
    //SETUP user HUD
    [_heart1 setPosition:ccp(_screenWidth / 2 - ( [_heart1 boundingBox].size.width ), _screenHeight -  10)];
    [_heart2 setPosition:ccp(_screenWidth / 2, _screenHeight - 10)];
    [_heart3 setPosition:ccp(_screenWidth / 2 + ( [_heart1 boundingBox].size.width ), _screenHeight - 10)];
    
    _scoreDisplay.position = ccp(_screenWidth - 10, _screenHeight - (_highScoreDisplay.boundingBox.size.height + 15));
    _highScoreDisplay.position = ccp(_screenWidth - 10, _screenHeight - 10);
    _highScoreText.position = ccp(_screenWidth - 100, _screenHeight - 10);
    
    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"highScore"];
    highScore = [currentHighScore integerValue];
    
    _highScoreDisplay.string = [NSString stringWithFormat:@"%li", highScore];
    
}

-(void)update:(CCTime)delta
{
    
    if ([_player.physicsBody sleeping] || [_player position].x < _screenWidth / 3) {
        [_player.physicsBody setSleeping:NO];
        [_player.physicsBody setVelocity:ccp(5.0f, [_player.physicsBody velocity].y)];
    }else if ([_player position].x > _screenWidth / 2){
        [_player.physicsBody setVelocity:ccp(0, [_player.physicsBody velocity].y)];
    }
    
    //We aren't reversing time
    if (!_isReversingTime) {
        [self moveWorldForwardWithDelta:delta];
    }//If we are reversing time
    else{
        [self reverseWorldWithDelta:delta];
    }
    
    //Delete all the unecessary sprites
    for (CCSprite *sprite in spritesToDelete) {
        [sprite removeFromParentAndCleanup:YES];
    }
    if ([spritesToDelete count] > 0) {
        [spritesToDelete removeAllObjects];
    }
    
    //If player has fallen too far or if player is completely off screen end the game
    if (_player.position.y < WORLDBOUND || _player.position.x <  - _player.boundingBox.size.width) {
        [self endgame];
    }
    
    //Let the player know how much time they have to reverse
    [_timeBar setScaleX: reverseTimeRemaining / MAXREVERSETIME];
    ccColor3B color = reverseTimeRemaining / MAXREVERSETIME == 1 ? ccBLUE : ccRED;
    [_timeBar setColor:[CCColor colorWithCcColor3b:color]];
}

//+=+=+=+ HANDLE TOUCHES +=+=+=+=+
-(void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    if ([touch locationInWorld].x < (_screenWidth / 2) ) {
        if ( !(_isReversingTime) && !(_isPlayerJumping) && _isPlayerWalking && !_hasPlayerUsedSpring) {
            remainingJumpSteps = MAXJUMPSTEPS;
            _isPlayerJumping = YES;
            jumpTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(playerJump) userInfo:nil repeats:YES];
            _isPlayerWalking = NO;
            //Make the player looks like he's jumping
            
            [_player performSelector:@selector(startJumpAnimation) withObject:nil afterDelay: 0.05];
            
            //Play jump sound
            AudioServicesPlaySystemSound(jumpSound);
        }
    }
    else{
        if (_isReverseEnabled) {
            if (reverseTimeRemaining == MAXREVERSETIME ){
                _isReversingTime = YES;
                // Automatically set the timer to half of what the max is so people can't spam clones
                reverseTimeRemaining = MAXREVERSETIME / 1.5;
                
                //Add copy of player
                [self addPlayerClone];
                
                if(!isMusicPaused){
                    //Stop playing regular music and play reversed
                    [_audioPlayer pause];
                    if ([_reverseAudioPlayer prepareToPlay]) {
                        [_reverseAudioPlayer play];
                    }
                }
            }
        }
        
    }
}

-(void) touchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (_isReversingTime) {
        if (!isMusicPaused) {
            //Play regular music again
            if ([_reverseAudioPlayer isPlaying]) {
                [_reverseAudioPlayer pause];
            }
            
            if ([_audioPlayer prepareToPlay]) {
                [_audioPlayer play];
            }
            
        }
        
        _isReversingTime = NO;
    }
    if (!_isPlayerJumping) {
        _isPlayerJumping = YES;
    }
    if (jumpTimer != nil) {
        [jumpTimer invalidate];
        jumpTimer = nil;
    }
    
}

-(void)touchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (_isReversingTime) {
        if (!isMusicPaused) {
            //Play regular music again
            if ([_reverseAudioPlayer isPlaying]) {
                [_reverseAudioPlayer pause];
            }
            
            if ([_audioPlayer prepareToPlay]) {
                [_audioPlayer play];
            }
        }
        _isReversingTime = NO;
        
    }
    if (!_isPlayerJumping) {
        _isPlayerJumping = YES;
    }
    if (jumpTimer != nil) {
        [jumpTimer invalidate];
        jumpTimer = nil;
    }
}

//+=+=+=+ HANDLE TOUCHES +=+=+=+=+

-(void) playerJump
{
    //If the player is actually touching something, they can jump
    if (remainingJumpSteps > 0 && !_hasPlayerUsedSpring) {
        [_player.physicsBody applyForce:ccp(0, 775.0f)];
        remainingJumpSteps--;
    }
    else if (jumpTimer != nil){
        [jumpTimer invalidate];
        jumpTimer = nil;
    }
}

-(void) fillBufferUp
{
    lastLevelNum = [self computeLevel];
    NSString *levelString = [NSString stringWithFormat:@"Levels/Level%i", lastLevelNum];
    if (timeSinceLastCoinLevel == 0) {
        BOOL gotLucky = arc4random() % 100 > 70;
        if (gotLucky) {
            levelString = [NSString stringWithFormat:@"Levels/CoinLevel"];
        }
        timeSinceLastCoinLevel = COINLEVELTIME;
    }
    
    CCNode *level = [CCBReader load:levelString];
    for (CCSprite *sprite in level.children) {
        [spritesToAdd addObject:sprite];
    }
    
    for (CCSprite* sprite in spritesToAdd) {
        [sprite removeFromParent];
        if ([sprite isKindOfClass:TopBlock.class]) {
            //Add a random environment sprite if the price is right
            if (arc4random() % 50 < 7) {
                CCNode * randomEnvironment = [self getRandomEnvironmentSprite];
                randomEnvironment.position = ccp([sprite position].x, [sprite position].y + [sprite boundingBox].size.height);
                [_levelBuffer addChild:randomEnvironment];
            }
        }
        [_levelBuffer addChild:sprite];
    }
    
    [spritesToAdd removeAllObjects];
}

-(void) addPlayerClone
{
    CCNode *playerClone = [CCBReader load:@"PlayerClone"];
    CGPoint clonePos = ccp(_player.position.x - (_player.boundingBox.size.width / 2 + 15), _player.position.y - _player.boundingBox.size.height / 2);
    playerClone.position = clonePos;
    
    [_levelBuffer addChild:playerClone];
}

-(void) addRandomBackgroundImage
{
    NSString *cloudStr = [NSString stringWithFormat:@"Cloud%i", arc4random() % 3 + 1];
    CCNode *cloud = [CCBReader load:cloudStr];
    float ratio = (float)(50.0 / (arc4random() % 50 + 50));
    cloud.position = ccp(_screenWidth + [cloud boundingBox].size.width, _screenHeight * ratio);
    [_backgroundNode addChild:cloud];
}

-(void) drawInitialBackgroundObjects
{
    for (int i = 0 ; i < 10; i++) {
        float ratioX = (float)(50.0 / (arc4random() % 50 + 50));
        float ratioY = (float)(50.0 / (arc4random() % 50 + 50));
        NSString *cloudStr = [NSString stringWithFormat:@"Cloud%i", arc4random() % 3 + 1];
        CCNode *cloud = [CCBReader load:cloudStr];
        cloud.position = ccp(_screenWidth * ratioX, _screenHeight * ratioY);
        [_backgroundNode addChild:cloud];
    }
}

//+++++++++++++ WORLD UPDATE CALLS +++++++++++++++
-(void) reverseWorldWithDelta:(CCTime) delta
{
    //Don't show that we are recharging
    [_rechargeText setVisible:NO];
    
    //Remove from the time we have to reverse
    if (reverseTimeRemaining > delta) {
        reverseTimeRemaining -= delta;
    }
    else{ //If you don't have more "charge" to go back in time set back to normal
        _isReversingTime = NO;
        reverseTimeRemaining = 0;
        if (!isMusicPaused) {
            if ([_reverseAudioPlayer isPlaying]) {
                [_reverseAudioPlayer pause];
            }
            
            if ([_audioPlayer prepareToPlay]) {
                [_audioPlayer play];
            }
        }
    }
    
    for (CCSprite *sprite in _levelBuffer.children) {
        if ([sprite isKindOfClass: StaticSprite.class]) {
            //Move static sprites back
            [(StaticSprite*)sprite update:delta withSpeed:-speed];
        }
        else if ([sprite isKindOfClass:TimeDependentSprite.class]){
            //MAKE TIME DEPENDENT SPRITE GO BACK HERE
            [(TimeDependentSprite *) sprite goBackInTime];
            [sprite setRotation:0];
        }
        // Delete sprites that can't be shown on screen
        if (sprite.position.x < -speed * MAXREVERSETIME) {
            [spritesToDelete addObject:sprite];
        }
    }
    
    //Update background objects
    for (BackgroundSprite * sprite in [_backgroundNode children]) {
        [sprite update:delta withSpeed:-speed];
        //Make sure that we clean up the sprites
    }
}

-(void) moveWorldForwardWithDelta:(CCTime) delta
{
    if (!isMusicPaused) {
        //Assure that correct music is playing
        if ([_reverseAudioPlayer isPlaying]) {
            [_reverseAudioPlayer pause];
            if ([_audioPlayer prepareToPlay]) {
                [_audioPlayer play];
            }
        }
    }
    
    //--Update timers--
    if (_playerHurtTimer > delta) {
        _playerHurtTimer -= delta;
    }
    else{
        _playerHurtTimer = 0;
    }
    
    if (timeUntilSpeedUp > delta) {
        timeUntilSpeedUp -= delta;
    }
    else{
        timeUntilSpeedUp = TIMEUNILSPEEDUP;
        speed += SPEEDINCREASE;
    }
    
    if (_playerInvulnerableTimer > delta) {
        _playerInvulnerableTimer -= delta;
    }
    else{
        _playerInvulnerableTimer = 0;
    }
    
    if (timeSinceLastCoinLevel > delta) {
        timeSinceLastCoinLevel -= delta;
    }
    else{
        timeSinceLastCoinLevel = 0;
    }
    
    if (timeUntilDifficultyIncrease > delta) {
        timeUntilDifficultyIncrease -= delta;
    }
    else{
        timeUntilDifficultyIncrease = DIFFICULTYINCREASETIME;
        difficultyThreshold++;
    }
    //--Update timers--
    
    //Randomly add background images , but don't want to have too many
    if ((int)[[_backgroundNode children] count] < 10) {
        if (arc4random() % 1000 < 10 ) {
            [self addRandomBackgroundImage];
        }
    }
    
    
    //If we're not reversing and the timer is still up, subtract the time elapsed
    //and show the text that you are recharging or not
    if (reverseTimeRemaining < MAXREVERSETIME) {
        [_rechargeText setVisible:YES];
        reverseTimeRemaining += 1.5 * delta;
    }
    else{ //Set it back to the maximum so we don't continuously add it
        reverseTimeRemaining = MAXREVERSETIME;
        [_rechargeText setVisible:NO];
    }
    
    //Move all of the static sprites to the left
    for (CCSprite *sprite in _levelBuffer.children) {
        if ([sprite isKindOfClass: StaticSprite.class]) {
            [(StaticSprite*)sprite update:delta withSpeed:speed];
            if (sprite.position.x > lastBlockPos) {
                lastBlockPos = sprite.position.x;
            }
        }
        else if ([sprite isKindOfClass:TimeDependentSprite.class]){
            [(TimeDependentSprite *) sprite saveCurrentPosition];
            //Manually setting it to zero since the set rotation doesn't work
            [sprite setRotation:0];
            if ([sprite isKindOfClass:Enemy.class]) {
                [sprite.physicsBody setVelocity:ccp( -speed * 1.2, [sprite.physicsBody velocity].y)];
                //                    CCLOG(@"Enemy speed is %f", (-speed * delta) * 100);
            }
        }
        
        if (sprite.position.x < sprite.position.x < -speed * MAXREVERSETIME) {
            [spritesToDelete addObject:sprite];
        }
    }
    
    //Update background objects
    for (BackgroundSprite * sprite in [_backgroundNode children]) {
        [sprite update:delta withSpeed:speed];
        //Make sure that we clean up the sprites
        if ([sprite position].x < WORLDBOUND / 20) {
            [spritesToDelete addObject:sprite];
        }
    }
    
    //Resetting last block position every time and filling array up if we need to
    if (lastBlockPos < 600) {
        [self fillBufferUp];
    }
    lastBlockPos = 0;
}
//+++++++++++++ WORLD UPDATE CALLS +++++++++++++++




//++++++COLLISION CALLS +++++++++
-(void) makePlayerJump
{
    [_player startJumpAnimation];
    _isPlayerWalking = NO;
}

-(void) hurtPlayer
{
    //Play hurt sound
    AudioServicesPlaySystemSound(hurtSound);
    
    [_player startHurtAnimation];
    _playerHurtTimer = PLAYERHURTTIME;
    _playerInvulnerableTimer = INVULNERABLETIME;
    _playerWasHurt = YES;
    [self handlePlayerHealthDisplay];
}
-(void) collectGem
{
    score += 100;
    NSString *scoreText = [NSString stringWithFormat:@"%li", score];
    _scoreDisplay.string = scoreText;
    if (score > highScore) {
        _highScoreDisplay.string = scoreText;
    }
    AudioServicesPlaySystemSound(gemGrabSound);
}
-(void) collectCoin
{
    score += 25;
    NSString *scoreText = [NSString stringWithFormat:@"%li", score];
    _scoreDisplay.string = scoreText;
    if (score > highScore) {
        _highScoreDisplay.string = scoreText;
    }
    AudioServicesPlaySystemSound(coinGrabSound);
}
-(void) collectStarAtPos:(CGPoint) pos
{
    score += 1000;
    NSString *scoreText = [NSString stringWithFormat:@"%li", score];
    _scoreDisplay.string = scoreText;
    if (score > highScore) {
        _highScoreDisplay.string = scoreText;
    }
    
    CCParticleSystem *explosion = (CCParticleSystem *)[CCBReader load:@"Effects/StarSparkles"];
    explosion.autoRemoveOnFinish = YES;
    explosion.position = pos;
    [_physicsNode addChild:explosion];
    AudioServicesPlaySystemSound(starGrabSound);
}
-(void) makePlayerWalk
{
    [_player startWalkAnimation];
}
-(void)killEnemy
{
    score += 200;
    NSString *scoreText = [NSString stringWithFormat:@"%li", score];
    _scoreDisplay.string = scoreText;
    if (score > highScore) {
        _highScoreDisplay.string = scoreText;
    }
    AudioServicesPlaySystemSound(enemyKillSound);
}
-(void) superJump
{
    AudioServicesPlaySystemSound(superSpringSound);
}
-(void) playerUsedSpring
{
    AudioServicesPlaySystemSound(springJumpSound);
}
//---------COLLISION CALLS ----------

-(void) pushPlayerDown
{
    if (!_isPlayerWalking || _isPlayerJumping) {
        [_player.physicsBody applyImpulse:ccp(0, -50000.0)];
    }
}

-(void) removeEnemySprite:(id) sprite
{
    [sprite removeFromParentAndCleanup:YES];
}

-(CCNode *) getRandomEnvironmentSprite
{
    CCNode * sprite;
    int rand = arc4random() % 6;
    switch (rand) {
        case 0:
            sprite = [CCBReader load:@"Environment/BushSprite"];
            break;
        case 1:
            sprite = [CCBReader load:@"Environment/MushroomBrown"];
            break;
        case 2:
            sprite = [CCBReader load:@"Environment/MushroomRed"];
            break;
        case 3:
            sprite = [CCBReader load:@"Environment/Plant"];
            break;
        case 4:
            sprite = [CCBReader load:@"Environment/PlantPurple"];
            break;
        default:
            sprite = [CCBReader load:@"Environment/Rock"];
            break;
    }
    return sprite;
}

-(void) loadSounds
{
    //Jump
    NSString *path = [[NSBundle mainBundle] pathForResource:@"jump" ofType:@"mp3"];
    NSURL *pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &jumpSound);
    
    //Hurt
    path = [[NSBundle mainBundle] pathForResource:@"hit" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &hurtSound);
    
    //Coin
    path = [[NSBundle mainBundle] pathForResource:@"coin_grab" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &coinGrabSound);
    
    //Killing enemy
    path = [[NSBundle mainBundle] pathForResource:@"enemy_kill" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &enemyKillSound);
    
    //Getting gem
    path = [[NSBundle mainBundle] pathForResource:@"gem_grab" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &gemGrabSound);
    
    //Getting star
    path = [[NSBundle mainBundle] pathForResource:@"star_grab" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &starGrabSound);
    
    //Spring jump
    path = [[NSBundle mainBundle] pathForResource:@"spring_jump" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &springJumpSound);
    //Super Spring Jump
    path = [[NSBundle mainBundle] pathForResource:@"super_spring" ofType:@"wav"];
    pathURL = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &superSpringSound);
}

-(void) resetSpringTimer
{
    _hasPlayerUsedSpring = NO;
}

-(void) pauseMusic
{
    [_toggleMusicButton togglesSelectedState];
    isMusicPaused = !isMusicPaused;
    
    if (isMusicPaused) {
        if ([_audioPlayer isPlaying]) {
            [_audioPlayer pause];
        }
        if ([_reverseAudioPlayer isPlaying]) {
            [_reverseAudioPlayer pause];
        }
    }
    else{
        if (_isReversingTime) {
            if ([_audioPlayer isPlaying]) {
                [_audioPlayer pause];
            }
            if (![_reverseAudioPlayer isPlaying]) {
                if ([_reverseAudioPlayer prepareToPlay]) {
                    [_reverseAudioPlayer play];
                }
                
            }
        }
        else{
            if ([_reverseAudioPlayer isPlaying]) {
                [_reverseAudioPlayer pause];
            }
            if (![_audioPlayer isPlaying]) {
                if ([_audioPlayer prepareToPlay]) {
                    [_audioPlayer play];
                }
                
            }
        }
    }
}
-(void) handlePlayerHealthDisplay
{
    CCBAnimationManager* animationManager;
    switch ([_player health]) {
        case 6:
            
            break;
        case 5:
            animationManager = _heart3.userObject;
            [animationManager runAnimationsForSequenceNamed:@"halfheart"];
            animationManager = _heart2.userObject;
            [animationManager runAnimationsForSequenceNamed:@"fullheart"];
            break;
        case 4:
            animationManager = _heart3.userObject;
            [animationManager runAnimationsForSequenceNamed:@"emptyheart"];
            break;
        case 3:
            animationManager = _heart2.userObject;
            [animationManager runAnimationsForSequenceNamed:@"halfheart"];
            break;
        case 2:
            animationManager = _heart2.userObject;
            [animationManager runAnimationsForSequenceNamed:@"emptyheart"];
            animationManager = _heart1.userObject;
            [animationManager runAnimationsForSequenceNamed:@"fullheart"];
            break;
        case 1:
            animationManager = _heart1.userObject;
            [animationManager runAnimationsForSequenceNamed:@"halfheart"];
            break;
            
            //Player has died
        default:
            [self endgame];
            break;
    }
}

-(void) endgame
{
    [self stopMusic];
    
    //Store highscore information
    if (score >= highScore) {
        NSNumber *newHighScore = [NSNumber numberWithInt:score];
        [[NSUserDefaults standardUserDefaults] setObject:newHighScore forKey:@"highScore"];
    }
    
    CCScene *gameOver = [CCBReader loadAsScene:@"GameOver"];
    [[CCDirector sharedDirector] replaceScene:gameOver];
}

-(void) stopMusic
{
    if (_audioPlayer.isPlaying) {
        [_audioPlayer stop];
    }
    if (_reverseAudioPlayer.isPlaying) {
        [_reverseAudioPlayer stop];
    }
}

-(int) computeLevel
{
    int level = 1;
    
    float percentForEasyLevel = 1.0f / (float)difficultyThreshold;
    float randomPercent = 1.0 / (float)(arc4random() % difficultyThreshold + 1);
    
    if (percentForEasyLevel >= randomPercent) {
        level = arc4random() % 6 + 1;
    }
    else{
        float percentForMediumLevel = 1.0f / ( (float)difficultyThreshold / 2 );
        if (percentForMediumLevel >= randomPercent) {
            level = arc4random() % 6 + 5;
        }
        else{
            float percentForHarderLevel = 1.0f / ( (float)difficultyThreshold / 3 );
            if (percentForHarderLevel >= randomPercent) {
                level = arc4random() % 5 + 11;
            }
            else{
                level = arc4random() % 5 + 14;
            }
        }
    }
    
    return level;
}


@end
