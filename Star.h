//
//  Star.h
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/30/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "StaticSprite.h"

@interface Star : StaticSprite

@end
