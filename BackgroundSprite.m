//
//  BackgroundSprite.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/28/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "BackgroundSprite.h"

@implementation BackgroundSprite
{
    float _viewScale;
}
-(void) didLoadFromCCB
{
    _viewScale = (float)(1.0f / (arc4random() % 10 + 3));
    [self setScaleX:10 * _viewScale];
    [self setScaleY:10 * _viewScale];
}

-(void) update:(CCTime)delta withSpeed:(double)speed
{
    [super update:delta withSpeed:_viewScale * speed];
}

@end
