//
//  Constants.m
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/15/14.
//
//

#import "Constants.h"

@implementation Constants

int const MAXARRAYSIZE = 500;
int const WORLDBOUND = -500;
int const PTM_RATIO = 30;

int const PLAYERTAG = 0;
int const BLOCKTAG = 1;
int const CLONETAG = 2;
int const ENEMYTAG = 3;

int const CLONEOPACITY = 125;
int const MAXJUMPSTEPS = 15;
double const INITIALSPEED = 100;
float const GRAVITY = 10.0f;
float const MAXREVERSETIME = 6.0f;
float const TIMEUNILSPEEDUP = 15.0f;
float const SPEEDINCREASE = 7.0f;
float const INVULNERABLETIME = 1.0f;
float const PLAYERHURTTIME = .5;
float const COINLEVELTIME = 30.0f;
float const DIFFICULTYINCREASETIME = 10.0f;

float const FIXED_TIMESTEP = 1.0f / 45.0f;

// Minimum remaining time to avoid box2d unstability caused by very small delta times
// if remaining time to simulate is smaller than this, the rest of time will be added to the last step,
// instead of performing one more single step with only the small delta time.
float const MINIMUM_TIMESTEP = 1.0f / 600.0f;

int const VELOCITY_ITERATIONS = 5;
int const POSITION_ITERATIONS = 10;

// maximum number of steps per tick to avoid spiral of death
int const MAXIMUM_NUMBER_OF_STEPS = 25;


@end
