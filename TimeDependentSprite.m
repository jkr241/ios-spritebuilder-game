//
//  TimeDependent.m
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/15/14.
//
//

#import "TimeDependentSprite.h"

@implementation TimeDependentSprite

{
    NSMutableArray *positionArray;
    NSMutableArray *velocityArray;
}
-(void) didLoadFromCCB
{
    positionArray = [NSMutableArray array];
    velocityArray = [NSMutableArray array];
}


-(void) saveCurrentPosition
{
    //Store the current position as an NSValue since CGPoints cannot be saved
    NSValue *positionVal = [NSValue valueWithCGPoint:self.position];
    NSValue *velocityVal = [NSValue valueWithCGPoint:[self physicsBody].velocity];
    
    if ( (int)[positionArray count] < MAXARRAYSIZE ) {
        [positionArray addObject:positionVal];
        [velocityArray addObject:velocityVal];
    }//If we have taken up all of the available positions delete the first one and add current pos
    //to the array
    else if ( (int)[positionArray count] == MAXARRAYSIZE ){
        [positionArray removeObjectAtIndex:0];
        [positionArray addObject:positionVal];
        [velocityArray removeObjectAtIndex:0];
        [velocityArray addObject:velocityVal];
    }
    
    
}

-(void) goBackInTime
{
    //If we have positions available to pick from
    if ( (int)[positionArray count] > 0 ) {
        //Get the last position in the position array and set it to the player's position
        NSValue *lastPosition = [positionArray lastObject];
        NSValue *lastVelocity = [velocityArray lastObject];
        self.position = [lastPosition CGPointValue];
        [self.physicsBody setVelocity:[lastVelocity CGPointValue]];
        [positionArray removeLastObject];
        [velocityArray removeLastObject];
    }
    
}




@end
