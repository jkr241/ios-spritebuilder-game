//
//  Coin.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/28/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Coin.h"

@implementation Coin

-(void) didLoadFromCCB
{
    self.physicsBody.collisionType = @"coin";
    self.physicsBody.collisionGroup = [StaticSprite noCollideGroup];


}

@end
