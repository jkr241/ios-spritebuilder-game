//
//  TutorialScene.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/30/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "TutorialScene.h"

@implementation TutorialScene
{
    BOOL explainedEnemies;
    BOOL explainedReverseTime;
    BOOL explainedSwipeDown;
    BOOL explainedSprings;
    
    CCLabelTTF *_textExplanation;
    CCNodeGradient *_leftScreenGradient;
    CCNodeGradient *_rightScreenGradient;
    CCNode *_arrow;
    
    //Things that trigger world events
    CCNode *_jumpBlock;
    CCNode *_checkpointBlock1;
    CCNode *_checkpointBlock2;
    CCNode *_checkpointBlock3;
    CCNode *_finalBlock;
    CCNode *_firstEnemy;
}

-(void) didLoadFromCCB
{
    [super didLoadFromCCB];
    [super setIsReverseEnabled:NO];
    _leftScreenGradient.contentSize = CGSizeMake([super screenWidth] / 2, [super screenHeight]);
    _rightScreenGradient.contentSize = CGSizeMake([super screenWidth] / 2, [super screenHeight]);
    _rightScreenGradient.position = ccp([super screenWidth] / 2, 0);
    _textExplanation.position = ccp([super screenWidth] / 2, [super screenHeight] / 1.25);
//    [self hideLeftGradient];
    [self displayLeftGradient];
    [_arrow setVisible:NO];
    [_textExplanation setString:@"Press and hold the left side of the screen to jump."];
}

-(void) update:(CCTime)delta
{
    if ([super player].position.x < 0) {
        [super player].position = ccp([super screenWidth] / 5, [super player].position.y);
    }
    if (_jumpBlock.position.x < 0 && !explainedEnemies) {
        [self explainEnemies];
        [self performSelector:@selector(finishExplainEnemies) withObject:nil afterDelay:5.0];
    }
    
    if(_checkpointBlock1.position.x < 0 && !explainedSwipeDown){
        [self explainSwipeDown];
        [self performSelector:@selector(finishExplainSwipeDown) withObject:nil afterDelay:5.0];
    }
    
    if (_checkpointBlock2.position.x < 0 && !explainedReverseTime) {
        [self explainReverseTime];
        [self performSelector:@selector(finishExplainReverseTime) withObject:nil afterDelay:5.0];
    }
    if (_checkpointBlock3.position.x < 0 && !explainedSprings)
    {
        [self explainSprings];
        [self performSelector:@selector(finishExplainSprings) withObject:nil afterDelay:5.0];
    }
    if (_finalBlock.position.x < 0) {
        [self endTutorial];
    }
    [super update:delta];
    
}

-(void) displayLeftGradient
{
    [_leftScreenGradient setVisible:YES];
}

-(void) hideLeftGradient
{
    [_leftScreenGradient setVisible:NO];
}

-(void) displayRightGradient
{
    [_rightScreenGradient setVisible:YES];
}

-(void) hideRightGradient
{
    [_rightScreenGradient setVisible:NO];
}

-(void) restartUpdate
{
    [[CCDirector sharedDirector] resume];
}

-(void) stopUpdate
{
    [[CCDirector sharedDirector] pause];
}

-(void) finishExplainEnemies
{
    [self restartUpdate];
    explainedEnemies = YES;
}

-(void) explainEnemies
{
    [self stopUpdate];
    [_textExplanation setString: @"Watch out for enemies. \n Be sure to either avoid them of just jump on their heads to kill them."];
}

-(void) explainSwipeDown
{
    [self stopUpdate];
    [_textExplanation setString:@"If you find yourself going too high, swipe down on the left side of the screen to push yourself down."];
    CCParticleSystem *swipeIndicator = (CCParticleSystem * )[[CCBReader reader] load:@"Effects/SwipeIndicator"];
    swipeIndicator.autoRemoveOnFinish = YES;
    swipeIndicator.position = ccp([super screenWidth] / 4, [super screenHeight] / 1.25);
    swipeIndicator.physicsBody.velocity = ccp(0, -200);
    [[super physicsNode] addChild:swipeIndicator];
}
-(void) finishExplainSwipeDown
{
    [self restartUpdate];
    explainedSwipeDown = YES;
}

-(void) explainReverseTime
{
    [super setIsReverseEnabled:YES];
    [self stopUpdate];
    [self hideLeftGradient];
    [self displayRightGradient];
    [_textExplanation setString:@"Press the right side of the screen when the time meter is full to go back in time.\n You will leave a copy of yourself that you can use to your advantage.\n"];
    [_arrow setVisible:YES];
}

-(void) finishExplainReverseTime
{
    explainedReverseTime = YES;
    [_arrow setVisible:NO];
    [self restartUpdate];
    
}

-(void) explainSprings
{
    [self stopUpdate];
    [_textExplanation setString: @"Jump on springs for a boost.\n Falling on them will give you an extra boost."];
}

-(void) finishExplainSprings
{
    [self restartUpdate];
    explainedSprings = YES;
}

-(void) endTutorial
{
    [self stopUpdate];
    [_textExplanation setString: @"Now, go and play the real game."];
    [self performSelector:@selector(goToMainMenu) withObject:nil afterDelay:3.0];
    
}

-(void) goToMainMenu
{
    [super stopMusic];
    CCScene *mainMenu = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:mainMenu];
}
@end
