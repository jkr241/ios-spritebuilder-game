//
//  Constants.h
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/15/14.
//
//


@interface Constants : NSObject 

extern const int MAXARRAYSIZE;
extern const int WORLDBOUND;
extern const int PTM_RATIO;

extern const int BLOCKTAG;
extern const int PLAYERTAG;
extern const int ENEMYTAG;
extern const int CLONETAG;

extern const int CLONEOPACITY;
extern const int MAXJUMPSTEPS;
extern const double INITIALSPEED;
extern const float GRAVITY;
extern const float MAXREVERSETIME;
extern const float TIMEUNILSPEEDUP;
extern const float SPEEDINCREASE;
extern const float INVULNERABLETIME;
extern const float PLAYERHURTTIME;
extern const float COINLEVELTIME;
extern const float DIFFICULTYINCREASETIME;

extern float const FIXED_TIMESTEP;

// Minimum remaining time to avoid box2d unstability caused by very small delta times
// if remaining time to simulate is smaller than this, the rest of time will be added to the last step,
// instead of performing one more single step with only the small delta time.
extern float const MINIMUM_TIMESTEP;

extern int const VELOCITY_ITERATIONS;
extern int const POSITION_ITERATIONS;

// maximum number of steps per tick to avoid spiral of death
extern int const MAXIMUM_NUMBER_OF_STEPS;

@end
