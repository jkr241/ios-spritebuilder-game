//
//  Spike.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Spike.h"

@implementation Spike

-(void) didLoadFromCCB
{
    self.physicsBody.collisionType = @"spike";
}

@end
