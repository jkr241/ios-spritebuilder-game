//
//  CollisionHandler.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CollisionHandler.h"

@implementation CollisionHandler

{
    Gameplay *world;
}

-(id) initWithWorld:(Gameplay *) theWorld
{
    if (self = [super init]) {
        world = theWorld;
    }
    
    return self;
}

-(id) initWithTurorial:(TutorialScene *) tutorial
{
    if (self = [super init]) {
        world = tutorial;
    }
    
    return self;
}

/*
 *Collision handler for player with anything
 */
-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA topblock:(CCNode *)nodeB
{
    BOOL isPlayerJumping = [world isPlayerJumping];
    BOOL playerWasHurt = [world playerWasHurt];
    //If the player was jumping and they collide with something below, allow them to jump again
    if ( nodeA.position.y > nodeB.position.y && nodeA.position.x > nodeB.position.x && ( isPlayerJumping || playerWasHurt) ) {
        if(isPlayerJumping){
            world.isPlayerJumping = NO;
        }
        if (world.playerHurtTimer == 0) {
            if (playerWasHurt) {
                world.playerWasHurt = NO;
            }
            [world makePlayerWalk];
            world.isPlayerWalking = YES;
        }
    }
}

-(void) ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA enemy:(CCNode *)nodeB
{
    if (!world.isReversingTime) {
        //If player jumped on top of enemy
        if ([nodeA position].y - ([nodeA boundingBox].size.height / 2) > [nodeB position].y) {
            [nodeA.physicsBody setVelocity:ccp([nodeA.physicsBody velocity].x, 0)];
            [nodeA.physicsBody applyImpulse:ccp(0, 10000.0f)];
            world.isPlayerJumping = YES;
            world.isPlayerWalking = NO;
            
            [world makePlayerJump];
            [world killEnemy];
            
        }
        else{
            //PLAYER TAKES DAMAGE OR SOMETHING HERE
            [nodeA.physicsBody applyImpulse:ccp(-10000.0f, 0)];
            world.isPlayerWalking = NO;
            
            [world hurtPlayer];
            
        }
        //Remove the dead enemy after a delay
        nodeB.physicsBody.collisionMask = @[];
        [(Enemy *) nodeB startDeathAnimation];
        [nodeB performSelector:@selector(removeFromParentAndCleanup:) withObject:[NSNumber numberWithBool:YES] afterDelay:.5];
    }
}

//----Collect powerups----
-(BOOL)ccPhysicsCollisionPreSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA coin:(CCNode *)nodeB
{
    [nodeB removeFromParentAndCleanup:YES];
    [world collectCoin];
    return NO;
}

-(BOOL)ccPhysicsCollisionPreSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA gem:(CCNode *)nodeB
{
    [nodeB removeFromParentAndCleanup:YES];
    [world collectGem];
    return NO;
}
-(BOOL)ccPhysicsCollisionPreSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA star:(CCNode *)nodeB
{
    [world collectStarAtPos:nodeB.position];
    [nodeB removeFromParentAndCleanup:YES];
    
    return NO;
}
//----Collect powerups----

-(BOOL)ccPhysicsCollisionPreSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA spring:(CCNode *)nodeB
{
    if (![world isReversingTime] && ![world hasPlayerUsedSpring]) {
        
        world.isPlayerJumping = YES;
        float yBoost = 1.0f;
        
        if ([nodeA.physicsBody velocity].y < -.0001) {
            yBoost = 1.4;
            [world superJump];
        }
        else{
            [world playerUsedSpring];
        }
        //Clearing forces and velocity on player
        [nodeA.physicsBody setVelocity:ccp([nodeA.physicsBody velocity].x, 0)];
        [nodeA.physicsBody setForce:ccp(0, 0)];
        
        [nodeA.physicsBody applyImpulse:ccp(0, 44000.0f * yBoost)];
        
        [(Spring *) nodeB springAnimation];
        
        //Set this so spring doesn't go off multiple times
        world.hasPlayerUsedSpring = YES;
        
        [world performSelector:@selector(resetSpringTimer) withObject:nil afterDelay:.5];
    }
    
    return YES;
}

-(void) ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair player:(CCNode *)nodeA spike:(CCNode *)nodeB
{
    if (!world.isReversingTime) {
        if (world.playerHurtTimer == 0 && world.playerInvulnerableTimer == 0) {
            [world hurtPlayer];
        }
        
        BOOL isPlayerJumping = [world isPlayerJumping];
        BOOL playerWasHurt = [world playerWasHurt];
        //If the player was jumping and they collide with something below, allow them to jump again
        if ( nodeA.position.y > nodeB.position.y && nodeA.position.x > nodeB.position.x && ( isPlayerJumping || playerWasHurt) ) {
            if(isPlayerJumping){
                world.isPlayerJumping = NO;
            }
            if (world.playerHurtTimer == 0) {
                if (playerWasHurt) {
                    world.playerWasHurt = NO;
                }
                [world makePlayerWalk];
                world.isPlayerWalking = YES;
            }
        }
        
    }
}

@end
