//
//  Enemy.m
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/28/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Enemy.h"

@implementation Enemy

-(void) didLoadFromCCB
{
    [super didLoadFromCCB];
    self.physicsBody.collisionType = @"enemy";
    self.physicsBody.collisionGroup = [StaticSprite noCollideGroup];
    [self performSelector:@selector(startIdleAnimaiton) withObject:nil afterDelay:.5];
}

-(void) startDeathAnimation
{
    // the animation manager of each node is stored in the 'userObject' property
    CCBAnimationManager* animationManager = self.userObject;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"dead"];
}

-(void) startIdleAnimaiton
{
    // the animation manager of each node is stored in the 'userObject' property
    CCBAnimationManager* animationManager = self.userObject;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"idle"];

}

@end
