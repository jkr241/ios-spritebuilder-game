//
//  Player.m
//  ParadoxJumper
//
//  Created by Jan Rodriguez on 1/15/14.
//
//

#import "Player.h"

@implementation Player

-(void) didLoadFromCCB
{
    [super didLoadFromCCB];
    _health = 6;
    self.physicsBody.collisionType = @"player";
}

- (void)startJumpAnimation
{
    // the animation manager of each node is stored in the 'userObject' property
    CCBAnimationManager* animationManager = self.userObject;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"jump"];
}

-(void)startWalkAnimation
{
    // the animation manager of each node is stored in the 'userObject' property
    CCBAnimationManager* animationManager = self.userObject;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"walk"];
}

-(void)startHurtAnimation
{
    _health--;
    // the animation manager of each node is stored in the 'userObject' property
    CCBAnimationManager* animationManager = self.userObject;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"damage"];
}


@end
