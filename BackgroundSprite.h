//
//  BackgroundSprite.h
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/28/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "StaticSprite.h"

@interface BackgroundSprite : StaticSprite

-(void) update:(CCTime)delta withSpeed:(double)speed;

@end
