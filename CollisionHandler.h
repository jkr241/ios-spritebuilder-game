//
//  CollisionHandler.h
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Gameplay.h"
#import "TutorialScene.h"

@interface CollisionHandler : NSObject <CCPhysicsCollisionDelegate>

-(id) initWithWorld: (Gameplay*) theWorld;

@end
