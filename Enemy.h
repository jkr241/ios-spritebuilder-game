//
//  Enemy.h
//  ParadoxRunner
//
//  Created by Jan Rodriguez on 1/28/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "TimeDependentSprite.h"
#import "StaticSprite.h"

@interface Enemy : TimeDependentSprite

-(void) startDeathAnimation;

@end
